# Microservice Course

## Java Microservices with Spring Boot and Spring Cloud

This repository is intended to record the knowledge acquired in the microservices course taught by Dr. Nélio Alves.

Nélio Alves is a software developer, professor and doctor in software engineering doctor with over 20 years of professional experience.

### Architecture

![Microservice Architecture](images/architecture-1.jpg "Microservice Architecture")

### Stack

<!-- TOC -->
* [Feign](#feign)
* [Ribbon](#ribbon)
* [Eureka Server](#eureka-server)
* [API Gateway Zuul](#api-gateway-zuul)
* [Hystrix](#hystrix)
* [Oauth2 and JWT](#oauth2-and-jwt)
* [Centralized Configuration](#centralized-configuration)
* [Docker](#docker)
<!-- TOC -->

### Conceptual Model

![Conceptual Model](images/conceptual-model-1.jpg "Conceptual Model")

### Authentication and Authorization

![Authentication and Authorization](images/authentication-authorization-1.jpg "Authentication and Authorization")

![Authentication and Authorization](images/authentication-authorization-2.jpg "Authentication and Authorization")

![Authentication and Authorization](images/authentication-authorization-3.jpg "Authentication and Authorization")

### Instructions

Instructions on how to get the project working will be provided later.